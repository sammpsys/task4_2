# Task 4.2
A repo that contains the graphical implementation of a quiz.

## Requirements

Requires `QTCreator`.

## Add your files


Clone the repo and open the project in QTCreator. Build and run the project in QTCreator.

## Maintainers

[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)

## Contributors and License

Copyright 2022,

[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)

