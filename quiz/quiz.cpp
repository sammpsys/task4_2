#include "quiz.h"
#include "ui_quiz.h"

#include <QApplication>
#include <QCheckBox>
#include <QComboBox>
#include <QCommandLinkButton>
#include <QDateTimeEdit>
#include <QDial>
#include <QDialogButtonBox>
#include <QFileSystemModel>
#include <QGridLayout>
#include <QGroupBox>
#include <QMenu>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QPlainTextEdit>
#include <QProgressBar>
#include <QPushButton>
#include <QRadioButton>
#include <QScrollBar>
#include <QShortcut>
#include <QSpinBox>
#include <QStandardItemModel>
#include <QStyle>
#include <QStyleFactory>
#include <QTextBrowser>
#include <QTreeView>
#include <QTableWidget>
#include <QTextEdit>
#include <QToolBox>
#include <QToolButton>

#include <QIcon>
#include <QDesktopServices>
#include <QScreen>
#include <QWindow>

#include <QDebug>
#include <QLibraryInfo>
#include <QSysInfo>
#include <QTextStream>
#include <QTimer>

quiz::quiz(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::quiz)
{
    ui->setupUi(this);
}

quiz::~quiz()
{
    delete ui;
}

