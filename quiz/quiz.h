#ifndef QUIZ_H
#define QUIZ_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class quiz; }
QT_END_NAMESPACE

class quiz : public QMainWindow
{
    Q_OBJECT

public:
    quiz(QWidget *parent = nullptr);
    ~quiz();

private:
    Ui::quiz *ui;
};
#endif // QUIZ_H
